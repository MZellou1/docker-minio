# MINIO server

### Connect server to the store

'''
sudo mount -v -t cifs -o rw,user=USER,domain=IGN,uid=UID_USER,gid=GID_USER,file_mode=0777,dir_mode=0777 //store/store-dai /mnt/store-dai
'''

### UP MINIO with docker

Previously fill the .env file with docker env vars.

'''
docker compose -f docker-compose-minio.yml up
'''

#### Sync folder from local machine to MINIO

1 - Setup alias for mc

mc alias set docker-minio http://172.20.0.61:9000/ $MINIO_ACCESS_KEY $MINIO_SECRET_KEY

2 - Sync local folder with bucket
mc mirror --watch /mnt/remote/TERRIA/DATASETS_DAI/LABELS_VECT/D004_2021/AA_S1_32/ docker-minio/ocsge/LABELS_VECTD004_2021/AA_S1_32/
mc mirror /mnt/remote/TERRIA/DATASETS_DAI/ docker-minio/ocsge/
mc mirror /mnt/remote/TERRIA/DATASETS_DAI/LABELS_VECT docker-minio/ocsge/LABELS_VECT
mc mirror /mnt/remote/TERRIA/DATASETS_DAI/ORTHOS docker-minio/ocsge/ORTHOS
mc mirror /mnt/remote/TERRIA/DATASETS_DAI/DEM docker-minio/ocsge/DEM
mc mirror /mnt/store-dai/tmp/20221108_3_COPC_Point_Clouds docker-minio/lidar
